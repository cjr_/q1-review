$(document).ready(function() {
  $('#searchButton').click(searchButtonClicked);
  // $('#searchButton').on('click', searchButtonClicked);
});

const CORS_PROXY = 'https://galvanize-cors.herokuapp.com/';
const RECIPE_PUPPY_API = `${CORS_PROXY}http://www.recipepuppy.com/api/?i=onions,garlic&p=1&q=`;

function searchButtonClicked() {
  // const attrTerm = $('#search').attr('class');
  // console.log(attrTerm);
  const searchTerm = $('#search').val();
  getSearchResults(searchTerm);
}

function getSearchResults(searchTerm) {
  const requestURL = `${RECIPE_PUPPY_API}${searchTerm}`;
  $.getJSON(requestURL, displayResults);
}

function displayResults(response) {
  var $recipes = $('#recipes');
  response.results.forEach((recipe) => {
    const $recipeElement = $(`
      <section class="row" data-ingredients="${recipe.ingredients}">
        <div class="col-xs-3">
          <img class="img-responsive" src="${recipe.thumbnail}">
        </div>
        <div class="col-xs-9">
          <h2>${recipe.title}</h2>
        </div>
      </section>`);

    $recipeElement.click(showIngredients);
    $recipes.append($recipeElement);
  });
}

function showIngredients() {
  $('#ingredients').html(`<p>${this.dataset.ingredients}</p>`);
}
